package gomech.dicegamegomec.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.Log;

public class AppStorePreferences {
    private volatile static SharedPreferences sharedPreferences;
    public static final String PREFERENCE_NAME = "dicegame_preferences";
    private AppStorePreferences() {
    }

    public static SharedPreferences getSharedPreferences(Context context) {
        if (sharedPreferences == null) {
            synchronized (AppStorePreferences.class) {
                if (sharedPreferences == null) {
                    sharedPreferences =
                            context.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
                }
            }
        }
        return sharedPreferences;
    }

    public static void putString(Context context, String key, String value) {
        if (context != null) {
            SharedPreferences.Editor edit = getSharedPreferences(context).edit();
            if (TextUtils.isEmpty(value)) {
                edit.remove(key);
            } else {
                edit.putString(key, value);
            }
            edit.apply();
        }
    }

    public static String getString(Context context, String key) {
        if (context != null) {
            return getSharedPreferences(context).getString(key, null);
        } else {
            return getString(context, key, null);
        }
    }

    public static String getString(Context context, String key, String def) {
        if (context != null) {
            return getSharedPreferences(context).getString(key, def);
        } else {
            return null;
        }
    }
}
