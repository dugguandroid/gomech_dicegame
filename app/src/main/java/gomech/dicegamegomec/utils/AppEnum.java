package gomech.dicegamegomec.utils;

public class AppEnum {
    private AppEnum() {
        throw new IllegalStateException(" AppEnum class");
    }
    public static final String FIRST_NAME = "first_name";
    public static final String SECOND_NAME = "second_name";
}
