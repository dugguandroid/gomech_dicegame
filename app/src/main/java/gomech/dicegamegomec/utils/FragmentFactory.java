package gomech.dicegamegomec.utils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import gomech.dicegamegomec.fragment.PlayDiceGameFragment;
import gomech.dicegamegomec.fragment.StartGamenFragment;

public class FragmentFactory {
    private FragmentFactory() {
        throw new IllegalStateException("FragmentFactory class");
    }
    public abstract static class Screens {
        private Screens() {
            throw new IllegalStateException("Screens class");
        }
        public static final String START_GAME = "START_GAME";
        public static final String PLAY_DICE_GAME = "PLAY_DICE_GAME";
    }

    /**
     * Returns fragment from base application
     */
    public static Fragment getFragment(String fragment, Bundle params) {
        Fragment contentFragment = null;
        switch (fragment) {
            case Screens.START_GAME:
                contentFragment = new StartGamenFragment();
                break;
            case Screens.PLAY_DICE_GAME:
                contentFragment = new PlayDiceGameFragment();
                break;
            default:
                break;
        }
        if (params != null && contentFragment != null) {
            contentFragment.setArguments(params);
        }
        return contentFragment;
    }

}