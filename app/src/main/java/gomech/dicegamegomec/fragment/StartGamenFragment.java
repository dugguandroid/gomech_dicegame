package gomech.dicegamegomec.fragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import gomech.dicegamegomec.DiceGameActivity;
import gomech.dicegamegomec.R;
import gomech.dicegamegomec.utils.AppEnum;
import gomech.dicegamegomec.utils.AppStorePreferences;
import gomech.dicegamegomec.utils.CommonUtils;
import gomech.dicegamegomec.utils.FragmentFactory;

public class StartGamenFragment extends Fragment implements View.OnClickListener {
    private TextView firstPlayer;
    private TextView secondPlayer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.start_game_with_user, container, false);
        firstPlayer = view.findViewById(R.id.firstPlayer);
        secondPlayer = view.findViewById(R.id.secondPlayer);
        TextView reset = view.findViewById(R.id.reset);

        if(AppStorePreferences.getString(getActivity(),AppEnum.FIRST_NAME,"").isEmpty() &&
                AppStorePreferences.getString(getActivity(),AppEnum.SECOND_NAME,"").isEmpty()) {
            reset.setVisibility(View.GONE);
            firstPlayer.setText(getString(R.string.select_first_name));
            secondPlayer.setText(getString(R.string.select_second_name));
        } else {
            reset.setVisibility(View.VISIBLE);
            if(!AppStorePreferences.getString(getActivity(),AppEnum.FIRST_NAME,"").isEmpty()) {
                firstPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_avatar_one,0, 0);
                firstPlayer.setText(AppStorePreferences.getString(getActivity(),AppEnum.FIRST_NAME,""));
            }
            if(!AppStorePreferences.getString(getActivity(),AppEnum.SECOND_NAME,"").isEmpty()) {
                secondPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_avatar_two, 0, 0);
                secondPlayer.setText(AppStorePreferences.getString(getActivity(), AppEnum.SECOND_NAME, ""));
            }
        }
        firstPlayer.setOnClickListener(this);
        secondPlayer.setOnClickListener(this);
        view.findViewById(R.id.startGame).setOnClickListener(this);
        view.findViewById(R.id.reset).setOnClickListener(this);
        return view;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        if(id == R.id.firstPlayer) {
            addPlayerName(getString(R.string.add_player_name),true);
        } else if(id == R.id.secondPlayer) {
            addPlayerName(getString(R.string.add_player_name),false);
        } else if(id == R.id.startGame) {
            if(AppStorePreferences.getString(getActivity(),AppEnum.FIRST_NAME,"").isEmpty() &&
                    AppStorePreferences.getString(getActivity(),AppEnum.SECOND_NAME,"").isEmpty()) {
                CommonUtils.showToast(getActivity(),getString(R.string.please_add_player_name));
            } else {
                Bundle bundle = new Bundle();
                bundle.putString(AppEnum.FIRST_NAME,AppStorePreferences.getString(getActivity(),AppEnum.FIRST_NAME,""));
                bundle.putString(AppEnum.SECOND_NAME,AppStorePreferences.getString(getActivity(),AppEnum.SECOND_NAME,""));
                DiceGameActivity.replaceToBackStack(getActivity(), FragmentFactory.getFragment(FragmentFactory.Screens.PLAY_DICE_GAME, bundle));
            }
        } else if(id == R.id.reset) {
            AppStorePreferences.putString(getActivity(),AppEnum.FIRST_NAME,"");
            AppStorePreferences.putString(getActivity(),AppEnum.SECOND_NAME,"");
            firstPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0,0,0, 0);
            secondPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, 0,0, 0);
            firstPlayer.setText(getString(R.string.select_first_name));
            secondPlayer.setText(getString(R.string.select_second_name));
        }
    }

    private void addPlayerName(String headerStr , final boolean isFirstName) {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View dialogView = factory.inflate(R.layout.add_player_name, null);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setView(dialogView);
        ((TextView)dialogView.findViewById(R.id.header)).setText(headerStr);
        final EditText playerName = dialogView.findViewById(R.id.playerName);
        dialogView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String playerNameStr= playerName.getText().toString().trim();
                if(playerNameStr.isEmpty()) {
                    CommonUtils.showToast(getActivity(),getString(R.string.name_cant_be_empty));
                } else {
                    if(isFirstName) {
                        AppStorePreferences.putString(getActivity(),AppEnum.FIRST_NAME,playerNameStr);
                        firstPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_avatar_one,0, 0);
                        firstPlayer.setText(playerNameStr);
                    } else {
                        AppStorePreferences.putString(getActivity(),AppEnum.SECOND_NAME,playerNameStr);
                        secondPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_avatar_two,0, 0);
                        secondPlayer.setText(playerNameStr);
                    }
                    dialog.dismiss();
                }
            }
        });
        dialogView.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }
    /***
     * override this and return true if child is handling backpress (Toolbar and
     * device back button)
     **/
    public boolean onPopBackStack () {
        return false;
    }

}
