package gomech.dicegamegomec.fragment;

import android.app.AlertDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

import gomech.dicegamegomec.DiceGameActivity;
import gomech.dicegamegomec.R;
import gomech.dicegamegomec.utils.AppEnum;
import gomech.dicegamegomec.utils.FragmentFactory;

public class PlayDiceGameFragment extends Fragment implements View.OnClickListener {
    private TextView firstPlayer;
    private TextView pointsFirstPlayer;
    private TextView secondPlayer;
    private TextView pointsSecondPlayer;
    private TextView hold;
    private TextView resetPlayer;
    private TextView roll;
    private TextView turnMSG;
    private View viewOne;
    private View viewTwo;
    private ImageView diceIcon;

    private Random random = new Random();
    private int num = 1;
    private boolean isFirstPlayerPlaying = false;
    private int totalCount = 0;
    private AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.game_layout, container, false);
        firstPlayer = view.findViewById(R.id.firstPlayer);
        viewOne = view.findViewById(R.id.viewOne);
        viewTwo = view.findViewById(R.id.viewTwo);
        pointsFirstPlayer = view.findViewById(R.id.pointsFirstPlayer);
        secondPlayer = view.findViewById(R.id.secondPlayer);
        resetPlayer = view.findViewById(R.id.resetPlayer);
        pointsSecondPlayer = view.findViewById(R.id.pointsSecondPlayer);
        hold = view.findViewById(R.id.hold);
        turnMSG = view.findViewById(R.id.turnMSG);
        roll = view.findViewById(R.id.roll);
        diceIcon = view.findViewById(R.id.diceIcon);
        Bundle bundle = getArguments();
        animation.setDuration(1000);
        if(bundle != null) {
            firstPlayer.setText(bundle.getString(AppEnum.FIRST_NAME,""));
            secondPlayer.setText(bundle.getString(AppEnum.SECOND_NAME,""));
            reset();
        }
        
        roll.setOnClickListener(this);
        hold.setOnClickListener(this);
        resetPlayer.setOnClickListener(this);
        return view;
    }

    private void reset() {
        isFirstPlayerPlaying = !isFirstPlayerPlaying;
        firstPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_avatar_one,0, 0);
        secondPlayer.setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_avatar_two,0, 0);
        pointsFirstPlayer.setText(getString(R.string.zero));
        pointsSecondPlayer.setText(getString(R.string.zero));
        String playerNameStr = isFirstPlayerPlaying ?
                firstPlayer.getText().toString().trim() : secondPlayer.getText().toString().trim();
        turnMSG.setText(getString(R.string.player_count,playerNameStr,getString(R.string.zero)));
        if(!isFirstPlayerPlaying) {
            viewTwo.setBackgroundColor(getResources().getColor(R.color.transparent));
            viewOne.setBackgroundColor(getResources().getColor(R.color.black_transparent));
        } else {
            viewOne.setBackgroundColor(getResources().getColor(R.color.transparent));
            viewTwo.setBackgroundColor(getResources().getColor(R.color.black_transparent));
        }
        totalCount = 0;
        diceIcon.setVisibility(View.GONE);
    }

    private void turnLost() {
        nextPlayerGame(getString(R.string.scored_on_this_turn, totalCount),false);
        totalCount = 0;
        diceIcon.setVisibility(View.GONE);
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.hold) {
            turnLost();
        } else if(view.getId() == R.id.resetPlayer) {
            DiceGameActivity.replaceToBackStack(getActivity(),FragmentFactory.getFragment(FragmentFactory.Screens.START_GAME, null));
        } else if(view.getId() == R.id.roll) {
            num = random.nextInt(6) + 1;
            animateDice(num);
            if(num == 1){
                turnLost();
            }else{
                totalCount += num;
                String playerNameStr = isFirstPlayerPlaying ?
                        firstPlayer.getText().toString().trim() : secondPlayer.getText().toString().trim();
                if(totalCount >= 100) {
                    nextPlayerGame(getString(R.string.you_won, playerNameStr),true);
                }
                turnMSG.setText(getString(R.string.player_count,playerNameStr,String.valueOf(totalCount)));
            }
            increaseScoreOfUser();
        }
    }

    private void nextPlayerGame(String headerStr,boolean isWon) {
        LayoutInflater factory = LayoutInflater.from(getActivity());
        final View dialogView = factory.inflate(R.layout.next_player_turn, null);
        final AlertDialog dialog = new AlertDialog.Builder(getActivity()).create();
        dialog.setCancelable(false);
        dialog.setView(dialogView);
        if(isWon) {
            ((TextView)dialogView.findViewById(R.id.header)).setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_win,0, 0);
        } else {
            ((TextView)dialogView.findViewById(R.id.header)).setCompoundDrawablesRelativeWithIntrinsicBounds(0, R.drawable.ic_lost_turn,0, 0);
        }
        ((TextView)dialogView.findViewById(R.id.header)).setText(headerStr);
        dialogView.findViewById(R.id.ok).setOnClickListener(v-> {
            if(isWon) {
                reset();
            } else {
                reset();
            }
            dialog.dismiss();
        });

        dialog.show();
    }

    private void increaseScoreOfUser () {
        if(isFirstPlayerPlaying) {
            pointsFirstPlayer.setText(String.valueOf(totalCount));
        } else {
            pointsSecondPlayer.setText(String.valueOf(totalCount));

        }
    }

    private void animateDice(int number){
        diceIcon.setVisibility(View.VISIBLE);
        if (number == 1) {
            diceIcon.setImageDrawable(getResources().getDrawable(R.drawable.dice_one));
        } else if (number == 2) {
            diceIcon.setImageDrawable(getResources().getDrawable(R.drawable.dice_two));

        } else if (number == 3) {
            diceIcon.setImageDrawable(getResources().getDrawable(R.drawable.dice_three));

        } else if (number == 4) {
            diceIcon.setImageDrawable(getResources().getDrawable(R.drawable.dice_four));

        } else if (number == 5) {
            diceIcon.setImageDrawable(getResources().getDrawable(R.drawable.dice_five));

        } else if (number == 6) {
            diceIcon.setImageDrawable(getResources().getDrawable(R.drawable.dice_six));
        }
    }

}
