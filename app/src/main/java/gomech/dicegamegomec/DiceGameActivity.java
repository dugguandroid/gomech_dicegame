package gomech.dicegamegomec;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import gomech.dicegamegomec.utils.CommonUtils;
import gomech.dicegamegomec.utils.FragmentFactory;

public class DiceGameActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dice_game);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        exitAppHandler = new Handler();
        getSupportActionBar().setTitle(getString(R.string.app_name));
        replaceToBackStack(this, FragmentFactory.getFragment(FragmentFactory.Screens.START_GAME, null));
    }


    public static void replaceToBackStack (FragmentActivity activity, Fragment fragment) {
        clearBackStack(activity);
        activity.getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(fragment.getClass().getName()).commit();
    }

    public static void clearBackStack (FragmentActivity activity) {
        try {
            final FragmentManager fragmentManager = activity.getSupportFragmentManager();
            while (fragmentManager.getBackStackEntryCount() != 0) {
                fragmentManager.popBackStackImmediate();
            }
        } catch (Exception e) {
            Log.d("clearBackStack",e.getMessage()+"");
        }
    }

    private Runnable cancelFinish = () -> shouldFinish = false;

    private boolean shouldFinish;
    protected boolean isFinished = false;
    private Handler exitAppHandler;
    @Override
    public void onBackPressed() {
        if (shouldFinish) {
            exitAppHandler.removeCallbacks(cancelFinish);
            isFinished = true;
            finish();
        } else {
            shouldFinish = true;
            exitAppHandler.postDelayed(cancelFinish, 2000);
            CommonUtils.showToast(this, getResources().getString(R.string.exit_app));
        }
    }

}
